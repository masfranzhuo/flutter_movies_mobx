import 'package:flutter/material.dart';
import 'package:movie/features/app/di/di.dart';

import 'features/app/app.dart';

void main() {
  DI.init();
  runApp(const MyApp());
}
