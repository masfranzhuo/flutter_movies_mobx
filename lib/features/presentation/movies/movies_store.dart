import 'package:mobx/mobx.dart';

import '../../domain/entity/movie.dart';

part 'movies_store.g.dart';

// ignore: library_private_types_in_public_api
class MoviesStore = _MoviesStore with _$MoviesStore;

abstract class _MoviesStore with Store {
  List<Movie> movies = [];

  @observable
  List<Movie> filteredMovies = [];

  @action
  void fetch() {
    movies = dummyMovies;
    filteredMovies = movies.toList();
  }

  @action
  void add(Movie movie) {
    movies.add(movie);
    filteredMovies = movies.toList();
  }

  @action
  void update(Movie movie) {
    movies[movies.indexWhere((e) => e.id == movie.id)] = movie;
    filteredMovies = movies.toList();
  }

  @action
  void delete(String id) {
    movies.removeWhere((e) => e.id == id);
    filteredMovies = movies.toList();
  }

  @action
  void searchByTitle(String query) {
    filteredMovies = movies
        .where((e) => e.title.toLowerCase().contains(query.toLowerCase()))
        .toList();
  }
}

List<Movie> dummyMovies = [
  Movie(
    id: '1',
    title: 'Abra Kadabra',
    director: 'Matheus',
    summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    genres: [Genre.action],
  ),
  Movie(
    id: '2',
    title: 'Abra Selamet',
    director: 'Nunes',
    summary: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    genres: [Genre.action, Genre.animation],
  ),
];
