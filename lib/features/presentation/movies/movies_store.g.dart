// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movies_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$MoviesStore on _MoviesStore, Store {
  late final _$filteredMoviesAtom =
      Atom(name: '_MoviesStore.filteredMovies', context: context);

  @override
  List<Movie> get filteredMovies {
    _$filteredMoviesAtom.reportRead();
    return super.filteredMovies;
  }

  @override
  set filteredMovies(List<Movie> value) {
    _$filteredMoviesAtom.reportWrite(value, super.filteredMovies, () {
      super.filteredMovies = value;
    });
  }

  late final _$_MoviesStoreActionController =
      ActionController(name: '_MoviesStore', context: context);

  @override
  void fetch() {
    final _$actionInfo =
        _$_MoviesStoreActionController.startAction(name: '_MoviesStore.fetch');
    try {
      return super.fetch();
    } finally {
      _$_MoviesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void add(Movie movie) {
    final _$actionInfo =
        _$_MoviesStoreActionController.startAction(name: '_MoviesStore.add');
    try {
      return super.add(movie);
    } finally {
      _$_MoviesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void update(Movie movie) {
    final _$actionInfo =
        _$_MoviesStoreActionController.startAction(name: '_MoviesStore.update');
    try {
      return super.update(movie);
    } finally {
      _$_MoviesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete(String id) {
    final _$actionInfo =
        _$_MoviesStoreActionController.startAction(name: '_MoviesStore.delete');
    try {
      return super.delete(id);
    } finally {
      _$_MoviesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void searchByTitle(String query) {
    final _$actionInfo = _$_MoviesStoreActionController.startAction(
        name: '_MoviesStore.searchByTitle');
    try {
      return super.searchByTitle(query);
    } finally {
      _$_MoviesStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
filteredMovies: ${filteredMovies}
    ''';
  }
}
