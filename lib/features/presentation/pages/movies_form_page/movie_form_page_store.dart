import 'package:mobx/mobx.dart';

import '../../../domain/entity/movie.dart';

part 'movie_form_page_store.g.dart';

// ignore: library_private_types_in_public_api
class MovieFormPageStore = _MovieFormPageStore with _$MovieFormPageStore;

abstract class _MovieFormPageStore with Store {
  @observable
  String title = '';

  @observable
  String director = '';

  @observable
  String summary = '';

  @observable
  List<Genre> genres = ObservableList<Genre>();

  @action
  void setForm(Movie movie) {
    title = movie.title;
    director = movie.director;
    summary = movie.summary;
    genres = ObservableList<Genre>.of(movie.genres);
  }

  @action
  void pressGenre(Genre genre) {
    if (genres.contains(genre)) {
      genres.removeWhere((e) => e == genre);
    } else {
      genres.add(genre);
    }
  }
}
