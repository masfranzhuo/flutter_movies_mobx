import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

import '../../../domain/entity/movie.dart';
import '../../movies/movies_store.dart';
import 'movie_form_page_store.dart';

@RoutePage()
class MovieFormPage extends StatefulWidget {
  const MovieFormPage({super.key, this.movie});

  final Movie? movie;

  @override
  State<MovieFormPage> createState() => _MovieFormPageState();
}

class _MovieFormPageState extends State<MovieFormPage> {
  final _store = MovieFormPageStore();
  final _moviesStore = GetIt.I<MoviesStore>();

  bool get isEditState => widget.movie != null;

  @override
  void initState() {
    if (isEditState) {
      _store.setForm(widget.movie!);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          if (isEditState)
            IconButton(
              onPressed: () {
                _moviesStore.delete(widget.movie!.id);
                context.router.back();
              },
              icon: const Icon(Icons.delete),
            ),
          IconButton(
            onPressed: () {
              if (isEditState) {
                _moviesStore.update(
                  Movie(
                    id: widget.movie!.id,
                    title: _store.title,
                    director: _store.director,
                    summary: _store.summary,
                    genres: _store.genres,
                  ),
                );
              } else {
                _moviesStore.add(
                  Movie(
                    id: DateTime.now().millisecondsSinceEpoch.toString(),
                    title: _store.title,
                    director: _store.director,
                    summary: _store.summary,
                    genres: _store.genres,
                  ),
                );
              }
              context.router.back();
            },
            icon: const Icon(Icons.save),
          ),
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: [
          Observer(
            builder: (_) {
              return TextFormField(
                initialValue: _store.title,
                decoration: const InputDecoration(label: Text('Title')),
                onChanged: (value) => {_store.title = value},
              );
            },
          ),
          Observer(builder: (_) {
            return TextFormField(
              initialValue: _store.director,
              decoration: const InputDecoration(label: Text('Director')),
              onChanged: (value) => {_store.director = value},
            );
          }),
          Observer(builder: (_) {
            return TextFormField(
              initialValue: _store.summary,
              decoration: const InputDecoration(label: Text('Summary')),
              onChanged: (value) => {_store.summary = value},
              maxLines: 3,
              maxLength: 100,
            );
          }),
          Observer(builder: (_) {
            return Wrap(
              children: Genre.values
                  .map((e) => GestureDetector(
                      onTap: () {
                        _store.pressGenre(e);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(right: 4),
                        child: Chip(
                          label: Text(e.value),
                          backgroundColor: _store.genres.contains(e)
                              ? Theme.of(context).colorScheme.primary
                              : Theme.of(context).colorScheme.primaryContainer,
                          side: BorderSide(
                            color: Theme.of(context).colorScheme.primary,
                          ),
                        ),
                      )))
                  .toList(),
            );
          }),
        ],
      ),
    );
  }
}
