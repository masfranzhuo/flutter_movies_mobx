import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:movie/features/app/routes/app_routes.dart';
import 'package:movie/features/presentation/widgets/movie_widget.dart';

import '../../movies/movies_store.dart';

@RoutePage()
class MoviesPage extends StatefulWidget {
  const MoviesPage({super.key});

  @override
  State<MoviesPage> createState() => _MoviesPageState();
}

class _MoviesPageState extends State<MoviesPage> {
  final _moviesStore = GetIt.I<MoviesStore>();

  @override
  void initState() {
    _moviesStore.fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Movies Collection'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          context.router.push(MovieFormRoute());
        },
        child: const Icon(Icons.add),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16),
        child: Column(children: [
          CupertinoTextField(
            placeholder: 'Search by Title',
            onChanged: (value) {
              _moviesStore.searchByTitle(value);
            },
          ),
          const SizedBox(height: 8),
          Observer(builder: (_) {
            if (_moviesStore.filteredMovies.isEmpty) {
              return const Text('No movie found');
            }

            return ListView.builder(
              shrinkWrap: true,
              itemCount: _moviesStore.filteredMovies.length,
              itemBuilder: (context, index) {
                return MovieWidget(movie: _moviesStore.filteredMovies[index]);
              },
            );
          })
        ]),
      ),
    );
  }
}
