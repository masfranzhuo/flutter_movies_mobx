import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../app/routes/app_routes.dart';
import '../../domain/entity/movie.dart';

class MovieWidget extends StatelessWidget {
  const MovieWidget({super.key, required this.movie});

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.router.push(MovieFormRoute(movie: movie));
      },
      child: Card(
        child: Column(
          children: [
            ListTile(
              title: Text(movie.title),
              subtitle: Text(movie.director),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Text(
                  movie.genres.map((e) => e.value).join(', '),
                  textAlign: TextAlign.right,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
