import 'package:get_it/get_it.dart';
import 'package:movie/features/presentation/movies/movies_store.dart';

class DI {
  static init() {
    GetIt.instance.registerLazySingleton(() => MoviesStore());
  }
}
