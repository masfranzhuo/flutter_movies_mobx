import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../domain/entity/movie.dart';
import '../../presentation/pages/movies_form_page/movie_form_page.dart';
import '../../presentation/pages/movies_page/movies_page.dart';

part 'app_routes.gr.dart';

@AutoRouterConfig()
class AppRoutes extends _$AppRoutes {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(initial: true, page: MoviesRoute.page),
        AutoRoute(page: MovieFormRoute.page),
      ];
}
