// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_routes.dart';

abstract class _$AppRoutes extends RootStackRouter {
  // ignore: unused_element
  _$AppRoutes({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    MovieFormRoute.name: (routeData) {
      final args = routeData.argsAs<MovieFormRouteArgs>(
          orElse: () => const MovieFormRouteArgs());
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: MovieFormPage(
          key: args.key,
          movie: args.movie,
        ),
      );
    },
    MoviesRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const MoviesPage(),
      );
    },
  };
}

/// generated route for
/// [MovieFormPage]
class MovieFormRoute extends PageRouteInfo<MovieFormRouteArgs> {
  MovieFormRoute({
    Key? key,
    Movie? movie,
    List<PageRouteInfo>? children,
  }) : super(
          MovieFormRoute.name,
          args: MovieFormRouteArgs(
            key: key,
            movie: movie,
          ),
          initialChildren: children,
        );

  static const String name = 'MovieFormRoute';

  static const PageInfo<MovieFormRouteArgs> page =
      PageInfo<MovieFormRouteArgs>(name);
}

class MovieFormRouteArgs {
  const MovieFormRouteArgs({
    this.key,
    this.movie,
  });

  final Key? key;

  final Movie? movie;

  @override
  String toString() {
    return 'MovieFormRouteArgs{key: $key, movie: $movie}';
  }
}

/// generated route for
/// [MoviesPage]
class MoviesRoute extends PageRouteInfo<void> {
  const MoviesRoute({List<PageRouteInfo>? children})
      : super(
          MoviesRoute.name,
          initialChildren: children,
        );

  static const String name = 'MoviesRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
