enum Genre {
  drama('Drama'),
  action('Action'),
  animation('Animation'),
  scifi('Sci-Fi'),
  horror('Horror');

  const Genre(this.value);

  final String value;
}

class Movie {
  final String id;
  final String title;
  final String director;
  final String summary;
  final List<Genre> genres;

  Movie({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.genres,
  });
}
